import React from 'react'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Legend  } from 'recharts';
import windowSize from 'react-window-size';
  
const sample = [
    { name: '29', my: 100, a1: 80 },
    { name: '31', my: 100, a1: 80 },
    { name: '02', my: 120, a1: 100 },
    { name: '06', my: 120, a1: 120 },
    { name: '07', my: 130, a1: 120 },
]

function PercGraphP({series = sample, windowWidth}) {
    return !!windowWidth && <LineChart
    width={(windowWidth < 769 ? windowWidth : windowWidth / 3) - 40}
    height={200}
    data={series}
    margin={{
      top: 5, right: 0, left: 0, bottom: 5,
    }}
  >
    <CartesianGrid strokeDasharray="3 3" />
    <XAxis dataKey="name" />
    <YAxis domain={['dataMin - 20', 'dataMax + 20']}/>
    <Legend />
    <Line type="monotone" name="desempenho (%)" dataKey="my" stroke="#82ca9d" isAnimationActive={false} />
  </LineChart>
}

export default windowSize(PercGraphP)

/*
    <Line type="monotone" name="Ana" dataKey="a1" stroke="#8884d8" isAnimationActive={false} />
*/
