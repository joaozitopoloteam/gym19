import React from 'react'

export default function BlockP({label, children}) {
    return <div className="panel for-block">
        <div className="panel-heading is-small">{label}</div>
        <div className="panel-block">{children}</div>
    </div>
}