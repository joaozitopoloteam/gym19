import React from 'react'
import IconP from './IconP';

export default function HeaderP({ hasBack, hasNext, title, children }) {
    return <>
        <div className="header-body">
            <IconP className="is-pulled-left" icon={hasBack ? 'fas fa-chevron-left' : ''} />
            <IconP className="is-pulled-right" icon={hasNext ? 'fas fa-chevron-right' : ''} />
            <div className="header-title">{title}</div>
        </div>
        {children && 
            <div className="header-children">
                <IconP className="is-pulled-left" icon="" />
                {children}
            </div>}
    </>

}