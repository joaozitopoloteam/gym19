import React from 'react'

export default function IconP({ className, icon }) {
    return <span className={`icon ${className}`}><i className={icon}/></span>
}
