import React from 'react'

export default function SplitP({ flexDirection = 'row', styles = [], children }) {
    return <div style={{ display: 'flex', flexDirection }}>
        {children.map((item, idx) => (
            <div style={styles[idx] || { flex: 1 }}>
                {item}
            </div>
        ))}
    </div>
}