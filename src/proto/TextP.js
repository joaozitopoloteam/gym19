import React from 'react'

export default function TextP({ bold, margin, children }) {
    return <div className={`has-text-centered ${bold ? 'is-bold' : ''} ${margin ? 'has-margin' : ''}`}>{children}</div>
}
