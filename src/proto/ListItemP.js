import React from 'react'
import IconP from './IconP';

export default function ListItemP({ title, hasNext, muted, children }) {
    return <div className="panel-block">
        <div>
            {title}
            {hasNext && <IconP icon="fas fa-caret-right" className="is-pulled-right" />}
        </div>
        <div className={`${muted ? 'has-text-grey' : ''}`}>
            {children}
        </div>
    </div>
}

ListItemP.GROUP = ({ label, children }) => (
    <div className="panel panel-list">
        {label && <div className="panel-heading is-small">{label}</div>}
        {children}
    </div>
)