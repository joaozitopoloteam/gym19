import React from 'react'

export default function InputFieldP({ placeholder }) {
    return <div className="columns is-gapless">
        <div className="column">
            <input type="text" className="input" placeholder={placeholder} />
        </div>
    </div>
}