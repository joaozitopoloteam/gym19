import React from 'react'

export default function ButtonP({ className, children }) {
    return <button className={`button is-fullwidth ${className}`}>{children}</button>
}
