import React from 'react';
import ButtonP from '../proto/ButtonP';
import HeaderP from '../proto/HeaderP';
import PercGraphP from '../proto/PercGraphP';
import TextP from '../proto/TextP';
import SpaceP from '../proto/SpaceP';

export default function S2() {
    return <>
        <HeaderP title="Academia ALJ" />
        <section>
            <TextP bold margin>Paulo Nunes</TextP>
            <PercGraphP/>
            <SpaceP/>
            <ButtonP>PLANOS</ButtonP>
        </section>
    </>
}