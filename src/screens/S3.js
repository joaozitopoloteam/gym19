import React from 'react';
import HeaderP from '../proto/HeaderP';
import ListItemP from '../proto/ListItemP';

export default function Home() {
    return <>
        <HeaderP title="Planos" hasBack/>
        <section>
            <ListItemP.GROUP>
                <ListItemP title="Pernas" hasNext>
                    <div>5 dias</div>
                </ListItemP>
                <ListItemP title="Torax" hasNext>
                    <div>3 dias</div>
                </ListItemP>
                <ListItemP title="Geral" hasNext>
                    <div>ontem</div>
                </ListItemP>
            </ListItemP.GROUP>
        </section>
    </>
}