import React from 'react';
import HeaderP from '../proto/HeaderP';
import IconP from '../proto/IconP';
import ButtonP from '../proto/ButtonP';

export default function S6() {
    return <>
        <HeaderP title="Plano" muted="5/5" hasBack>
            Pernas
        </HeaderP>
        <section>
            <div className="has-text-centered">
                <IconP className="is-large" icon="far fa-star fa-2x"/>
                <IconP className="is-large" icon="far fa-star fa-2x"/>
                <IconP className="is-large" icon="far fa-star fa-2x"/>
            </div>
            <div className="has-text-centered">
                <div className="is-size-2">FINALIZADO</div>
            </div>
            <div className="has-text-centered">
                <IconP className="is-large" icon="far fa-star fa-2x"/>
                <IconP className="is-large" icon="far fa-star fa-2x"/>
                <IconP className="is-large" icon="far fa-star fa-2x"/>
            </div>
            <ButtonP>PLANOS</ButtonP>
        </section>
    </>
}   