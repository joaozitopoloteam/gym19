import React from 'react';
import BlockP from '../proto/BlockP';
import ButtonP from '../proto/ButtonP';
import HeaderP from '../proto/HeaderP';

export default function S4() {
    return <>
        <HeaderP title="Plano" muted="0/5" hasBack>
            Pernas
        </HeaderP>
        <section>
            <ButtonP>INICIAR</ButtonP>
            <BlockP label="Leg Press">
                <div>3 séries</div>
                <div>10 repetições</div>
                <div>40kg</div>
            </BlockP>
            <BlockP label="Hack Squat">
                <div>3 séries</div>
                <div>10 repetições</div>
                <div>25kg</div>
            </BlockP>
            <BlockP label="Smith">
                <div>3 séries</div>
                <div>15 repetições</div>
                <div>10kg</div>
            </BlockP>
        </section>
    </>
}