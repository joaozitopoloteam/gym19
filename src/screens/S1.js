import React from 'react';
import ButtonP from '../proto/ButtonP';
import InputFieldP from '../proto/InputFieldP';
import TextP from '../proto/TextP';
import HeaderP from '../proto/HeaderP';
import SpaceP from '../proto/SpaceP';

export default function S1() {
    return <>
        <HeaderP title="Academia ALJ" />
        <section>
            <TextP bold margin>LOGIN</TextP>
            <InputFieldP placeholder="MATRÍCULA" />
            <InputFieldP placeholder="CPF" />
            <SpaceP/>
            <ButtonP>ENTRAR</ButtonP>
        </section>
    </>
}