import React from 'react'
import InputFieldP from '../proto/InputFieldP';
import ButtonP from '../proto/ButtonP';
import TextP from '../proto/TextP';
import IconP from '../proto/IconP';
import ListItemP from '../proto/ListItemP';
import BlockP from '../proto/BlockP';
import HeaderP from '../proto/HeaderP';
import PercGraphP from '../proto/PercGraphP';
import SpaceP from '../proto/SpaceP';
import Routes from '../Routes';

export default function Home() {
    return <>
        <Routes.MENU/>
        <SpaceP/>
        <HeaderP title="header" hasBack hasNext/>
        <SpaceP/>
        <HeaderP title="header two" hasBack>
            extra header information
        </HeaderP>
        <section>
            <InputFieldP placeholder="input test" />
            <ButtonP>OK</ButtonP>
            <TextP bold margin>TESTING TEXT</TextP>
            <div><IconP icon="fas fa-book-reader"/></div>
            <ListItemP.GROUP>
                <ListItemP title="list item" hasNext/>
                <ListItemP title="list item" hasNext muted>some text</ListItemP>
                <ListItemP title="list item" muted>some text</ListItemP>
            </ListItemP.GROUP>
            <ListItemP.GROUP label="actions">
                <ListItemP title="list item" hasNext/>
                <ListItemP title="list item" hasNext/>
            </ListItemP.GROUP>
            <BlockP label="small block">
                <div>content</div>
                <div>content</div>
            </BlockP>
            <PercGraphP/>
        </section>
    </>
}