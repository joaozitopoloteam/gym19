import React from 'react';
import BlockP from '../proto/BlockP';
import ButtonP from '../proto/ButtonP';
import HeaderP from '../proto/HeaderP';
import IconP from '../proto/IconP';
import SplitP from '../proto/SplitP';

export default function S5() {
    return <>
        <HeaderP title="Plano" muted="1/5" hasBack>
            Pernas
        </HeaderP>
        <section>
            <ButtonP>Parar</ButtonP>
            <BlockP label="Leg Press">
                <div className="is-pulled-right">
                    <IconP icon="fas fa-check-circle" />
                </div>
                <div>3 | 10 | 40kg</div>
            </BlockP>
            <BlockP label="Hack Squat">
                <SplitP styles={[undefined, { width: '4em' }]}>
                    {[
                        <>
                            <div>3 séries</div>
                            <div>10 repetições</div>
                            <div>25kg</div>
                        </>,
                        <>
                            <ButtonP className="no-gap">OK</ButtonP>
                            <ButtonP className="no-gap">Parcial</ButtonP>
                        </>
                    ]}
                </SplitP>
            </BlockP>
            <BlockP label="Smith">
                <SplitP styles={[undefined, { width: '4em' }]}>
                    {[
                    <>
                        <div>3 séries</div>
                        <div>10 repetições</div>
                        <div>10kg</div>
                    </>,
                        <>
                        <ButtonP className="no-gap">OK</ButtonP>
                        <ButtonP className="no-gap">Parcial</ButtonP>
                    </>
                    ]}
                </SplitP>
            </BlockP>
        </section>
    </>
}

