import '@fortawesome/fontawesome-free/css/all.css';
import 'bulma/css/bulma.min.css';
import React from 'react';
import './App.css';
import Routes from './Routes';

function App() {
  return <AppContainer>
    <Routes/>
  </AppContainer>
}

/** container for responsible visualization */
function AppContainer({children}) {
  return (
    <div className="columns">
      <div className="column is-4 is-offset-4">{children}</div>
    </div>
  )
}

export default App;
