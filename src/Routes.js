import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Home, S1, S2, S3, S4, S5, S6 } from './screens';

function Routes() {
  return <Router>
    <Route path="/" exact component={Home} />
    <Route path="/s1" exact component={S1} />
    <Route path="/s2" exact component={S2} />
    <Route path="/s3" exact component={S3} />
    <Route path="/s4" exact component={S4} />
    <Route path="/s5" exact component={S5} />
    <Route path="/s6" exact component={S6} />
  </Router>
}

Routes.MENU = () => (
    <div className="actions">
        <Link to="/s1">Login</Link>
        <Link to="/s2">Dash</Link>
        <Link to="/s3">Plans</Link>
        <Link to="/s4">Plan</Link>
        <Link to="/s5">Run</Link>
        <Link to="/s6">Finished</Link>
    </div>
)

export default Routes;
