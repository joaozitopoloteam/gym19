# gym19

## entities

	- users
	- exercises
	- equipments
	- plans
		- series (equipment, series, repetitions, weight)
		
		
## prototype

- input field OK
- button OK
- header (back button, forward button, muted second line, title, muted title) OK
- labeled block OK
- percentual graphic OK
- list item with proceed button OK
- multiline list item OK
- icon OK
- two line button NO
- centered text block OK

## prototype project

- simple routing
- proto components
- screen components
- bulma

## structure

- src
    - proto
    - 